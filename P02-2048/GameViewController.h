//
//  GameViewController.h
//  P02-2048
//
//  Created by 刘江韵 on 2017/2/7.
//  Copyright © 2017年 刘江韵. All rights reserved.
//

#ifndef GameViewController_h
#define GameViewController_h





#import <UIKit/UIKit.h>

@interface GameViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *number1;
@property (weak, nonatomic) IBOutlet UILabel *number2;
@property (weak, nonatomic) IBOutlet UILabel *number3;
@property (weak, nonatomic) IBOutlet UILabel *number4;
@property (weak, nonatomic) IBOutlet UILabel *number5;
@property (weak, nonatomic) IBOutlet UILabel *number6;
@property (weak, nonatomic) IBOutlet UILabel *number7;
@property (weak, nonatomic) IBOutlet UILabel *number8;
@property (weak, nonatomic) IBOutlet UILabel *number9;
@property (weak, nonatomic) IBOutlet UILabel *number10;
@property (weak, nonatomic) IBOutlet UILabel *number11;
@property (weak, nonatomic) IBOutlet UILabel *number12;
@property (weak, nonatomic) IBOutlet UILabel *number13;
@property (weak, nonatomic) IBOutlet UILabel *number14;
@property (weak, nonatomic) IBOutlet UILabel *number15;
@property (weak, nonatomic) IBOutlet UILabel *number16;
@property (weak, nonatomic) IBOutlet UILabel *score;


@end


#endif /* GameViewController_h */
