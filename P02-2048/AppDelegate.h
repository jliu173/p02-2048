//
//  AppDelegate.h
//  P02-2048
//
//  Created by 刘江韵 on 2017/2/7.
//  Copyright © 2017年 刘江韵. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

